#+title: Data Storage
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_INIT_OPTIONS: slideNumber:"c/t"
#+REVEAL_THEME: night
#+REVEAL_PLUGINS: notes
#+OPTIONS: toc:nil num:nil reveal_title_slide:"<H2>%t</H2><H3>%a</H3><p>%e</p><p>%d</p>"

* Data, information and Memory
** Concepts
Data is related to raw and unorganized sentences about facts.
Information is about data that is related with a context carring a meaning in its context.
Memory is related to the amount of information is stored in data structures. Its unit is the bit.
This relates with information theory where we learn about how we transfer, encode, decode, transform and store information.

[[file:img/information_theory_diagram.png]]

It also relates with the space concept from complexity theory, about the resource needed to represent the data.

* All about Data its usage and management
** Data types
The physical implementation of the data item is a series of bits; the actual format depends on the attribute’s data type. Typical data types in a relational database include numeric data types (e.g., integer and float), character strings of variable or fixed length (e.g., character or varchar), date and time-related data types (e.g., date, time, timestamp), and the Boolean data type to represent truth values.In many cases, the data types BLOB (binary large object) and CLOB (character large object) are also supported in order to capture large chunks of binary data and text data respectively.

** Data structures
One of the most important topics in CS, each data structure usually has its own study field.
- Array: limited collection of elements with same data type.
- Tuple: Collection of elements with different data type
- List, queues and stacks: Linked Collection of elements.
- Graphs or maps: hierarchical linked collection of elements. Includes tree and heaps.
- Tables: Collection of tuples.
- Hash tables: Tables with a graph structure to identify and facilitate the search of the tuples.
- Data Frames: hash table with additional operations.

Its importance is about how the structure helps with the process or computation (algorithms) the data is subject. We can trade space (memory  in a data structure) with time (computation) and viceversa when it is possible using a technique called memoization or cache.

** Data sources
It referes to the source of the facts the data represent. the sources can give us different data structures to represent the same facts. This concept relates with veracity and its the link with the real world.

** Data models
Data models is about which structures we are goin to use to store our data and also how those data structures are relationed.
- databases
  + relational
  + non-relational
- file systems

** Data management
This are the tools and frameworks we use to manage all the data problems we face.

* Data storage for big data

[[file:img/data_werehouse_and_lake.png]]

* Data storage and data importance
** 6 V of big data
*** Volume
- The name ‘Big Data’ itself is related to a size which is enormous.
- Volume is a huge amount of data.
- To determine the value of data, size of data plays a very crucial role. If the volume of data is very large, then it is actually considered as a ‘Big Data’. This means whether a particular data can actually be considered as a Big Data or not, is dependent upon the volume of data.
- Hence while dealing with Big Data it is necessary to consider a characteristic ‘Volume’.

Example: In the year 2016, the estimated global mobile traffic was 6.2 Exabytes (6.2 billion GB) per month. Also, by the year 2020 we will have almost 40000 Exabytes of data.

*** Velocity
- Velocity refers to the high speed of accumulation of data.
- In Big Data velocity data flows in from sources like machines, networks, social media, mobile phones etc.
- There is a massive and continuous flow of data. This determines the potential of data that how fast the data is generated and processed to meet the demands.
- Sampling data can help in dealing with the issue like ‘velocity’.

Example: There are more than 3.5 billion searches per day are made on Google. Also, Facebook users are increasing by 22%(Approx.) year by year.

*** Variety
- It refers to nature of data that is structured, semi-structured and unstructured data.
- It also refers to heterogeneous sources.
- Variety is basically the arrival of data from new sources that are both inside and outside of an enterprise. It can be structured, semi-structured and unstructured.
  + Structured data: This data is basically an organized data. It generally refers to data that has defined the length and format of data.
  + Semi- Structured data: This data is basically a semi-organised data. It is generally a form of data that do not conform to the formal structure of data. Log files are the examples of this type of data.
  + Unstructured data: This data basically refers to unorganized data. It generally refers to data that doesn’t fit neatly into the traditional row and column structure of the relational database. Texts, pictures, videos etc. are the examples of unstructured data which can’t be stored in the form of rows and columns.

*** Veracity
- It refers to inconsistencies and uncertainty in data, that is data which is available can sometimes get messy and quality and accuracy are difficult to control.
- Big Data is also variable because of the multitude of data dimensions resulting from multiple disparate data types and sources.
Example: Data in bulk could create confusion whereas less amount of data could convey half or Incomplete Information.

*** Value
- After having the 4 V’s into account there comes one more V which stands for Value! The bulk of Data having no Value is of no good to the company, unless you turn it into something useful.
- Data in itself is of no use or importance but it needs to be converted into something valuable to extract Information. Hence, you can state that Value! is the most important V of all the 6V’s.

*** Variability
- How fast or available data that extent is the structure of your data is changing?
- How often does the meaning or shape of your data change?

* ETL process and data pipelines
Data pipeline is a sequence of states applied to the data from a initial state to a goal state.

[[file:img/data_pipeline_example.png]]

ETL is an acronymous from Extract-Transform-Load is a data pipeline used before store the data using some extraction techniques from the raw data. ETL is a process to structure data.

[[file:img/etl.png]]
