#!/usr/bin/env python3
import pika
import time
import datetime
def send_message(connection: pika.BlockingConnection):
    channel = connection.channel()
    channel.queue_declare(queue='hello')
    for index in range(0,10):
        time.sleep(1)
        time_now = datetime.datetime.now()
        channel.basic_publish(exchange='', routing_key='hello', body=f'Hello from time: {time_now} with index: {index}')
        print(f"[x] Sent 'Hello {index}!'")

def collect_messages(connection: pika.BlockingConnection):
    channel = connection.channel()
    channel.queue_declare(queue='adios')
    def callback(ch, method, properties, body):
        print(f" [x] Received {body}", flush=True)


    channel.basic_consume(queue='adios', on_message_callback=callback, auto_ack=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

def process_messages(connection: pika.BlockingConnection):
    channel = connection.channel()
    channel.queue_declare(queue='hello')
    channel.queue_declare(queue='adios')

    def callback(ch, method, properties, body):
        time.sleep(5) # lest wait to figure it is processing something
        print(f" [x] Received {body}")
        ch.basic_publish(exchange='', routing_key='adios', body=f'Bye World {body[-25:]}!')
        print(f" [x] Sent '{body}'", flush=True)
        ch.basic_ack(delivery_tag = method.delivery_tag)

    channel.basic_consume(queue='hello', on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

modes= {'send': send_message,
        'collect': collect_messages,
        'process': process_messages}



if __name__=='__main__':
    import sys
    print('Service started', flush=True)
    declared_mode = sys.argv[1] if len(sys.argv) > 0 else  'send'
    seleced_mode = modes.get(declared_mode, send_message)
    print(f'using mode {declared_mode} from {sys.argv[1]}', flush=True)
    connection=pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    seleced_mode(connection=connection)
    connection.close()
