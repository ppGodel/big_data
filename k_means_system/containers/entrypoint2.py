#!/usr/bin/env python3

import pika
import time
import pandas as pd
import matplotlib.pyplot as plt
from typing import Tuple, List
import numpy as np
import json
from json import JSONEncoder

class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

def create_normal_distribution(mean, sd, size) -> np.array:
    return np.random.normal(loc=mean, scale=sd,size=size)

def create_distribution(mean: float, size: int) -> np.array:
    return create_normal_distribution(mean, mean * 0.25,size)


def generate_df(means: List[Tuple[float, float, str]], n: int) -> pd.DataFrame:
    lists = [
        (create_distribution(_x, n), create_distribution(_y, n), np.repeat(_l, n))
        for _x, _y, _l in means
    ]
    x = np.array([])
    y = np.array([])
    labels = np.array([])
    for _x, _y, _l in lists:
        x = np.concatenate((x, _x), axis=None)
        y = np.concatenate((y, _y))
        labels = np.concatenate((labels, _l))
    return pd.DataFrame({"x": x, "y": y, "label": labels})


def get_cmap(name="hsv"):
    """Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name."""
    return plt.colormaps.get_cmap(name)


def scatter_group_by(
    file_path: str, df: pd.DataFrame, x_column: str, y_column: str, label_column: str
):
    _, ax = plt.subplots()
    labels = pd.unique(df[label_column])
    cmap = get_cmap()
    for i, label in enumerate(labels):
        filter_df = df.query(f"{label_column} == '{label}'")
        ax.scatter(filter_df[x_column], filter_df[y_column], label=label)
    ax.legend()
    plt.set_cmap(cmap)
    plt.savefig(file_path)
    plt.close()


def euclidean_distance(p_1: np.array, p_2: np.array) -> float:
    return np.sqrt(np.sum((p_2 - p_1) ** 2))


def calculate_means(points: np.array, labels:np.array, clusters: int)-> np.array:
    return [np.mean(points[labels == k], axis=0) for k in range(clusters)]

def calculate_nearest_k(point: List, actual_means: List[List]):
    distance = [euclidean_distance(np.array(mean), np.array(point)) for mean in actual_means]
    nearest_k = np.argmin(distance)
    return nearest_k

def create_dataframe(points:np.array, labels:np.array, centroids)-> pd.DataFrame:
    df_points = pd.DataFrame(points, columns=['x','y'])
    df_points['label'] = np.array(['cluster_' + str(label) for label in labels])
    df_mean = pd.DataFrame(centroids, columns=['x','y'])
    df_mean['label'] =  ['centroid' for _ in range(len(centroids))]
    return pd.concat([df_points, df_mean])

def initialize():
    groups = [(20, 20, "cluster_0"), (300, 40, "cluster_1"), (200, 200, "cluster_2")]
    np.random.seed(0)
    df = generate_df(groups, 50)
    # print(f'df:{df}')
    scatter_group_by("img/clusters.png", df, "x", "y", "label")
    list_t = [
        (np.array(tuples[0:2]), tuples[2])
        for tuples in df.itertuples(index=False, name=None)
    ]
    points = [point for point, _ in list_t]
    return points


import pika
import time
def send_message(connection: pika.BlockingConnection):
    points = initialize()
    N = len(points)
    num_cluster = 3
    max_iterations = 15 #?

    x = np.array(points)
    y = np.random.randint(0, num_cluster, N)

    dimensions = len(points[0])
    old_mean = np.zeros((num_cluster, dimensions))
    actual_means = calculate_means(points=x, labels=y, clusters=num_cluster)


    channel = connection.channel()
    channel.queue_declare(queue='calculate_nearest_k')
    channel.queue_declare(queue='follow_up_step')

    problem_to_process_dict = {'iteration':0, 'points': len(x), 'clusters': num_cluster}
    problem_to_process_body=json.dumps(problem_to_process_dict, cls=NumpyArrayEncoder)
    channel.basic_publish(exchange='', routing_key='follow_up_step', body=problem_to_process_body)
    for point in x:
        # time.sleep(1)
        point_to_process_dict = {'point': point, 'actual_means': actual_means, 'iteration': 0}
        point_to_process_body=json.dumps(point_to_process_dict, cls=NumpyArrayEncoder)
        channel.basic_publish(exchange='', routing_key='calculate_nearest_k', body=point_to_process_body)
        print(f"[x] Sent '{point_to_process_body}'")

    # df = create_dataframe(x,y,actual_means)
    # scatter_group_by(file_path=f"img/kmeans_{t}.png", df=df,x_column="x", y_column="y", label_column='label')


def collect_messages(connection: pika.BlockingConnection):
    channel = connection.channel()
    channel.queue_declare(queue='follow_up_step')
    def callback(ch, method, properties, body):
        problem_dict = json.loads(body.decode('UTF-8'))
        print(f" [x] Received problem {body}", flush=True)
        def wait_and_send_new_iteration():

        ch.basic_consume(queue='labeled_point', on_message_callback=callback, auto_ack=True)


    channel.basic_consume(queue='follow_up_step', on_message_callback=callback, auto_ack=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

def process_messages(connection: pika.BlockingConnection):
    channel = connection.channel()
    channel.queue_declare(queue='calculate_nearest_k')
    channel.queue_declare(queue='adios')

    def callback(ch, method, properties, body):
        #time.sleep(5) # lest wait to figure it is processing something
        point_dict = json.loads(body.decode('UTF-8'))
        label = calculate_nearest_k(point=point_dict['point'], actual_means=point_dict['actual_means'])
        point_dict['label'] = int(label)
        body_resp=json.dumps(point_dict, cls=NumpyArrayEncoder)
        ch.basic_publish(exchange='', routing_key='labeled_point', body=body_resp)
        print(f" [x] Sent '{body_resp}'", flush=True)
        ch.basic_ack(delivery_tag = method.delivery_tag)

    channel.basic_consume(queue='calculate_nearest_k', on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

modes= {'send': send_message,
        'collect': collect_messages,
        'process': process_messages}



if __name__=='__main__':
    import sys
    print('Service started', flush=True)
    declared_mode = sys.argv[1] if len(sys.argv) > 0 else  'send'
    seleced_mode = modes.get(declared_mode, send_message)
    print(f'using mode {declared_mode} from {sys.argv[1]}', flush=True)
    connection=pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    seleced_mode(connection=connection)
    connection.close()
