#+title: Big Data
#+Author: José Anastacio Hernández Saldaña
#+email: jose.hernandezsal@uanl.edu.mx
#+Date: <2024-01-08 Mon>


* INFO
** Instructor
   José Anastacio Hernández Saldaña, M.C.
** Class Duration
   <2024-01-10 Wed>--<2024-03-20 Wed>
*** Holidays
1) Constitution day:: <2024-02-05 Mon>
2) Benito Juarez/Spring:: <2024-03-18 Mon>
3) Eastern vacations:: <2024-03-25 Mon>--<2024-04-06 Sat>
*** Final Presentation
<2024-04-10 Wed>
* References

* Week Schedule
1) Introduction, Big data and Data science world, from handcraft analysis to automated analysis
   - Class basics and score, class introduction and big data/data science world description
2) Computational Complexity
   - What are time and space in computer science?
   - How they impact big data and data science
3) Data storage and data importance
   - 6 V of big data
   - data models, data sources, data structures and data management
   - Data werehouse, data lakes, data shards
   - ETL
4) Online algorithms and distributed computation
   - How to handle big data, techniques and considerations
     + map-reduce
     + page rank
     + random walk
   - messaging
   - streaming
5) Data management tools
   - containers and virtualization
   - distributed computation frameworks in a technical view
   - cloud computation
6) Data pipelines
   - what is a data pipeline and how to build it.
7) Data analysis
   - How to run data analysis in big data? distributed programming in action!
     - messaging and events
     - online algoritms
     - memoization
     - data mining, Business intelligence
8) Machine learning for big data
   - what is Machine learning under the hood?
   - AI and ML
   - NN 101
9) Business and Data
   - Data decision for management
   - Business models
   - Data visualization
   - Data story telling
10) Proyect review


* Score
- 5 Homeworks              50 pts <2022-09-02 Fri>
- PIA                      50 pts <2022-11-11 Fri>
- Total                   100 pts
  *NO additional extra points*
